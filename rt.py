
import ruleEngine

ruleEngine.values["test"] = 10
ruleEngine.values["x"] = 0

ruleEngine.addRule( ["test == 11"], ["SET x 1"] )

#ruleEngine.addRule( ["test > 13"], ["SET x 2"] )

ruleEngine.addRule( ["test > 17"], ["SET y 10"] )

ruleEngine.addRule( ["test > 15", "x >= 1"], ["INC x 1"] )

print
print "::::::::::"
print

for i in range(9, 20):
	ruleEngine.values["test"] = i
	ruleEngine.checkRules()

	print ruleEngine.values
	

