K�kk� Fighter 2010-12-14 (Tomi Haapaniemi, 0903973, TT09S1E, tomi.haapaniemi@metropolia.fi )

Requirements (known to work):
	Python 2.6 (http://www.python.org/ftp/python/2.6.6/python-2.6.6.msi)
	Pygame 1.9.1 for python 2.6 (http://pygame.org/ftp/pygame-1.9.1.win32-py2.6.msi)

Start game: 
	run python main.py

Controls:
	Arrow keys for movement
	Spacebar for shooting
	
Mission:
	Destroy enemy ships.
	
