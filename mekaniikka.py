
import config
import math

class Vector():
	_angle = 0
	_magnitude = 0
	
	def __init__(self, angle, magnitude):
		self._angle = float(angle)
		self._magnitude = float(magnitude)
		
	def angle(self):
		return self._angle
	
	def magnitude(self):
		return self._magnitude
		
	def xy(self):
		x = self.magnitude() * math.cos( self.angle() )
		y = self.magnitude() * math.sin( self.angle() )
		return (x,y)
	def set(self, am):
		self._angle = am.angle()
		self._magnitude = am.magnitude()

class Velocity(Vector):
	def __init__(self, angle, magnitude):
		self._angle = angle
		self._magnitude = magnitude
		
	def addVelocity(self, nvv ):
		
		#Translate polar-cordinates to xy-cordinates
		i1 = self.xy()[0]
		j1 = self.xy()[1]

		i2 = nvv.xy()[0]
		j2 = nvv.xy()[1]
		
		# Sum the vectors
		i = i1 + i2
		j = j1 + j2

		#translate back to polar-cordinates
		#magnitude
		self._magnitude = math.sqrt(i**2 + j**2)
		#angle
		self._angle = math.atan2(j,i)
		
		if (self._magnitude > config.maxspeed):
			self._magnitude = config.maxspeed

	def effectForce(self, forceVector, mass):
		vv = Vector(forceVector.angle(), (config.timeunit * (forceVector.magnitude() / mass)) )
		self.addVelocity( vv )
		
	def getP(self, mass):
		return (mass*self.magnitude()*math.cos(self.angle()), mass*self.magnitude()*math.sin(self.angle()))
		
	def getVelocity(self):
		return Vector(self._angle, self._magnitude)
		
		
	class BasicObject:
		_mass = 0
		_velocity = 0
		
		def __init__(self, mass = 0, velocity = None):
			self._mass = mass
			self._velocity = velocity
		
		def mass(self):
			return _mass
		
		def velocity(self):
			return _velocity
		

