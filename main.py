## -*- coding: iso-8859-15 -*-
# Kokko Fighter
# made by Tomi Haapaniemi
# 0903973, TT09S1E

import pygame, sys,os
import menu
import world
import config

from pygame.locals import *
 
pygame.init() 
 
window = pygame.display.set_mode( config.resolution )
pygame.display.set_caption('Kokko Fighter')
screen = pygame.display.get_surface()

selection = None
while(True):
   selection = menu.menu(screen)
   
   if(selection == menu.START):
      world.startGame(screen)
      selection = None
      
   elif(selection == menu.QUIT):
      break
      pass
      
   else:
      pass
   
