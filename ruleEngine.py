

class RuleEngine():
	rules = []
	values = {}
	
	def __init__(self, values):
		self.values = values
	
	def addRule(self, clist , alist ):
		
		checklist = []
		actionlist = []
		
		for c in clist:
			temp = c.split(" ")
			if (temp[2].isdigit() ):
				temp[2] = int(temp[2])
			checklist.append( ( temp[0], temp[1], temp[2] ) )
		
		for a in alist:
			temp = a.split(" ")
			if (temp[2].isdigit() ):
				temp[2] = int(temp[2])
			actionlist.append( (temp[0], temp[1], temp[2]) )
			
		self.rules.append( (tuple(checklist), tuple(actionlist)) )
	
	def checkAndDoRule(self, checkable, actions ):

		rulecount = 0;

		#checkable = ((valuename operator b), (b operator c))
		#action =  ( (set x newvalue),  (set y newvalue2))
		
		#Check that all rules match
		for rule in checkable:
			#print rule[2], ":", values[ rule[0]]
			
			if (rule[1] == "=="):
				if( str( self.values[rule[0]] ) ==  str(rule[2]) ):
					rulecount += 1
			elif (rule[1] != "=="):
				if( str( self.values[rule[0]] ) !=  str(rule[2]) ):
					rulecount += 1
			elif (rule[1] == "<"):
				if( self.values[ rule[0]] < rule[2] ):
					rulecount += 1
			elif (rule[1] == "<="):
				if( self.values[ rule[0]] <= rule[2] ):
					rulecount += 1
			elif (rule[1] == ">"):
				if( self.values[ rule[0]] > rule[2] ):
					rulecount += 1
			elif (rule[1] == ">="):
				if( self.values[ rule[0]] >= rule[2] ):
					rulecount += 1

		#print rulecount, "," , len(checkable)
		
		if(rulecount == len(checkable) ):
			for a in actions:
				if(a[0] == "SET"):
					self.values[ a[1] ] = a[2]
				elif(a[0] == "INC"):
					self.values[ a[1] ] = self.values[a[1]]  + a[2]
				elif(a[0] == "DEC"):
					self.values[ a[1] ] = self.values[a[1]]  - a[2]
				elif(a[0] == "ADD"):
					if(self.values[ a[1] ] != None):
						self.values[ a[1] ].append(a[2])
					else:
						self.values[ a[1] ] = []
						self.values[ a[1] ].append(a[2])

	def checkRules(self):
		for r in self.rules:
			self.checkAndDoRule(r[0], r[1])

