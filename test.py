
import pygame
import gameEngine
import config
import math
import hud

from pygame.locals import *

pygame.init()

pygame.mixer.init()

pygame.mixer.music.load("sound/theseductionofdrpasteur.mp3")

#bgsound = pygame.mixer.Sound("sound/theseductionofdrpasteur.ogg")
w1 = pygame.mixer.Sound("sound/fusion.ogg")
w2 = pygame.mixer.Sound("sound/photon.ogg")
explosion = pygame.mixer.Sound("sound/Blast-SoundBible.com-2068539061.ogg")

print explosion.get_length()

objects = []

ship_surface = pygame.image.load("graphics/s1.png")
bg_surface = pygame.image.load("graphics/pleiades.jpg")

#for i in range(1,2):
#bg_surface = pygame.transform.scale2x(bg_surface)


GO_data = { "acceleration": 0.0, 
					"currentAngle": 0,
					"newAngle": 0,
					"x": 0,
					"y": 0,
					"turningSpeed": 360,
					"currentSpeed": 0,
					"maxSpeed": 100,
					"radius": 10,
					"mass":	20,
					"engineForce": 100,
					"ttl": -1,
					"target": None,
					"ai_target": "none",
					"ai_actions": None,
					"all_objects": objects,
					"distance_to_target": 2**31,
					"angle_to_target": 0
					}


x = gameEngine.Ship({ "acceleration": 0.001, 
					"currentAngle": 0,
					"newAngle": 0,
					"x": 0,
					"y": 0,
					"turningSpeed": math.radians(180),
					"currentSpeed": 0,
					"maxSpeed": 10,
					"radius": 10,
					"mass":	10,
					"engineForce": 100,
					"ttl": -1,
					"all_objects": objects,
					"ai_target": "none",
					"target": None,
					"distance_to_target": 2**31,
					"ai_actions": None,
					"angle_to_target": 0
					})

objects.append(x)
for a in range(1,5):
	objects.append( gameEngine.Ship( GO_data) )
	objects[a]._settings["x"] = 1000 * a 

screen = pygame.display.set_mode(config.resolution)
#screen = pygame.display.get_surface() 

pygame.display.set_caption('The Game')
pygame.mouse.set_visible(0)

clock = pygame.time.Clock()

keysDown = []
done = False;

removable = set()
i=0

pygame.mixer.music.play()

while not done:
	clock.tick(config.fps)
	i += 1
	
	if(i > int(config.fps / 2) ):
		i=0
	
	for o in objects:
		temp = o.update()
		if( isinstance(o, gameEngine.Ship) == True):
			o.generateShield()
			o.generateEnergy()
		
		if(o is not x):
			if( isinstance(o, gameEngine.Ammo) == False):
				if(i == 0):
#					o._ai.doRules()
					pass
				pass
		
		if (temp == False):
			removable.add(o)
			pass
			
			
	for o in removable:
		pygame.draw.circle(screen, (255,255,0), o.getRelativeLocation( x.xy(), (320+xx,240+yy) ) , o.radius() + 10, 0)
		
		if(isinstance(o, gameEngine.Ship )):
			#explosion.play()
			pass
		objects.remove(o)
		
	removable.clear()
		
	rad = 100
	xx = rad*math.cos( x.shipAngle() + math.radians(180) )
	yy = rad*math.sin( x.shipAngle() + math.radians(180) )
	
	pygame.display.flip()
	screen.fill( (0,0,0) )
	
	screen.blit(bg_surface, ( -(bg_surface.get_width() / 2.0) -x.x() / 5.00  , -(bg_surface.get_height() / 2.0) -x.y() / 5.00  )  )
	
	hud.drawStats(screen,x, x._settings["target"])
	
	
	#HUD
	distance_from_ship = 40
	hud_line_length =  20
	temp_loc = x.getRelativeLocation( x.xy() , (320+xx,240+yy) )
	hudline_x_start = temp_loc[0] + distance_from_ship * math.cos(x._settings["newAngle"])
	hudline_y_start = temp_loc[1] + distance_from_ship * math.sin(x._settings["newAngle"])
	
	hudline_x_stop = temp_loc[0] + (distance_from_ship + hud_line_length) * math.cos(x._settings["newAngle"])
	hudline_y_stop = temp_loc[1] + (distance_from_ship + hud_line_length )* math.sin(x._settings["newAngle"])
	
	extralines = []
	
	lines = 10
	hud_line_length =  5
	for n in range(1, lines):
		temp_line_x_start = temp_loc[0] + distance_from_ship * math.cos(x._settings["newAngle"] + math.radians((360.0/lines) * n))
		temp_line_y_start = temp_loc[1] + distance_from_ship * math.sin(x._settings["newAngle"] + math.radians((360.0/lines) * n))
		
		temp_x_stop = temp_loc[0] + (distance_from_ship + hud_line_length) * math.cos(x._settings["newAngle"] + math.radians((360.0/lines) * n))
		temp_y_stop = temp_loc[1] + (distance_from_ship + hud_line_length )* math.sin(x._settings["newAngle"] + math.radians((360.0/lines) * n))
	
		extralines.append( ( (temp_line_x_start, temp_line_y_start), (temp_x_stop, temp_y_stop)) )
		
	#draw Target line
	pygame.draw.line(screen, (255,0,0),  (hudline_x_start, hudline_y_start) , (hudline_x_stop, hudline_y_stop) , 3)
	
	for lines in extralines:
		pygame.draw.line(screen, (255,255,0),  lines[0] , lines[1] )
		
	
	hud_line_length =  20
	if(x._settings["target"] != None):
		temp1 = x._settings["target"].getRelativeLocation( x.xy() )
		angle1 = math.atan2(temp1[1], temp1[0])
		
		temp_x_start = temp_loc[0] + distance_from_ship * math.cos( angle1 ) 
		temp_y_start = temp_loc[1] + distance_from_ship  * math.sin( angle1 )
		
		temp_x_stop = temp_loc[0] + (distance_from_ship + hud_line_length) * math.cos( angle1 ) 
		temp_y_stop = temp_loc[1] + (distance_from_ship + hud_line_length) * math.sin( angle1 )
		
		pygame.draw.line(screen, (0,255,0),  (temp_x_start, temp_y_start) , (temp_x_stop, temp_y_stop) )
		
	
	
	ax = x.getRelativeLocation( x.xy() , (320+xx,240+yy) )[0] + 50 * math.cos(x._settings["newAngle"])
	ay = x.getRelativeLocation( x.xy() , (320+xx,240+yy) )[1] + 50 * math.sin(x._settings["newAngle"])
	
	bx = x.getRelativeLocation( x.xy() , (320+xx,240+yy) )[0] + (x._velocity.magnitude() / x._settings["maxSpeed"]) * 50 * math.cos(x._velocity.angle() )
	by = x.getRelativeLocation( x.xy() , (320+xx,240+yy) )[1] + (x._velocity.magnitude() / x._settings["maxSpeed"]) * 50 * math.sin(x._velocity.angle() )

	#pygame.draw.line(screen, (255,0,0), x.getRelativeLocation( x.xy(), (320+xx,240+yy) ), (ax, ay) )
	pygame.draw.line(screen, (0,0,255), x.getRelativeLocation( x.xy(), (320+xx,240+yy) ), (bx, by) )

	temp = objects[:]
	for o in objects:
		if( isinstance(o, gameEngine.Ship) == True):
			temp_s = ship_surface.copy()
			zoom = 3* float(o.radius()) / float(temp_s.get_height())
			temp_s = pygame.transform.rotozoom (temp_s, -math.degrees(o.shipAngle())-90, zoom)
			pygame.draw.circle(screen, (0,255,0), o.getRelativeLocation( x.xy(), (320+xx,240+yy) ) , o.radius(), 0)
			screen.blit(temp_s, o.getRelativeLocation( x.xy(), (320+xx -(temp_s.get_width() / 2) ,240+yy -(temp_s.get_height() / 2) ) ))
		else:
			pygame.draw.circle(screen, (0,255,0), o.getRelativeLocation( x.xy(), (320+xx,240+yy) ) , o.radius(), 0)

		temp.remove(o)
		
		hud.drawLines(screen, x.getRelativeLocation( x.xy(), (320+xx,240+yy)), x.shipAngle() , None )
		
		for o2 in temp:
			if(gameEngine.checkCollision(o,o2)):
				gameEngine.collide(o, o2)
				explosion.play()
				
				if(o.collide() == False):
					removable.add(o)
					pass
				if(o2.collide() == False):
					removable.add(o2)
					pass
				
				print "COLLISION"
				print
	
	
	#temp = gameEngine.GO( GO_data )
	#temp._x = x.x()
	#temp._y = x.y()
	#temp._radius = 3
	#objects.append(temp)
	
	for event in pygame.event.get():
		if event.type == KEYDOWN:
			keysDown.append(event.key)
		elif event.type == KEYUP:
			if event.key in keysDown:
				keysDown.remove(event.key)
		elif event.type == QUIT:
			done = True
			
	#print keysDown
	if K_UP in keysDown:
		x.thrust()
	if K_DOWN in keysDown:
		x.speedDown()
	if K_LEFT in keysDown:
		x.decreaseAngle()
#		x.thrust()
	if K_RIGHT in keysDown:
		x.increaseAngle()
#		x.thrust()
	if K_ESCAPE in keysDown:
		done = True
	if K_a in keysDown:
		x._ai.doRules()
		print x._settings["ai_target"], ":", x._settings["target"]
	if K_t in keysDown:
		x._ai.setTarget()
		print x._settings["target"]
		
	if K_SPACE in keysDown:
		if(x.depleteEnergy(10) == True):
			temp = x.shoot()
			objects.append(temp)
			while(gameEngine.checkCollision(x,temp)):
				temp.update()
			w2.play()
			
		



