

class Explosion():
    
    _spriteList = None
    _index = None
    _updates = None
    
    def __init__(self, spriteList, duration):
        
        self._index = 0
        
        #Duration is in msec -> let's convert to updates
        self._updates = config.fps * (duration / 1000)
        
        self._spriteList = spriteList
        
        pass
    
    def update(self):
        self._index += 1
        
        if(self._index < self._updates ):
            return True
        else:
            return False
    
    def getSprite(self):
        trueIndex = (self._index / self._updates) * (len(self._spriteList) -1)
        return self._spriteList[trueIndex]

