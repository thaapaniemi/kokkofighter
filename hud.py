# coding=ISO-8859-1

import pygame
import gameEngine
import config
import math
import world

from pygame.locals import *

line_distance_from_ship = 40

help_lines = 20
help_line_length =  5
help_line_width = 1
help_line_colour = (255,255,0)

aim_line_length =  20
aim_line_width = 3
aim_line_colour = (255,0,0)
	
target_line_length =  20
target_line_width = 20
target_line_colour = (0,255,0)

#speed_line_length =  0
speed_line_width = 1
speed_line_colour = (255,0,0)
	


def drawLines(screen, player_loc, player_angle, target_loc = None):
	
	#aim line
	aim_start  =	(	player_loc[0] + line_distance_from_ship * math.cos(player_angle),
						player_loc[1] + line_distance_from_ship * math.sin(player_angle)
						)
	
	aim_stop =	(	player_loc[0] + (line_distance_from_ship + aim_line_length) * math.cos(player_angle),
						player_loc[1] + (line_distance_from_ship + aim_line_length )* math.sin(player_angle)
					)
	
	
	
	#help lines
	helpLines = []
	for n in range(1, help_lines):
		temp_line_x_start = player_loc[0] + line_distance_from_ship * math.cos(player_angle + math.radians((360.0/help_lines) * n))
		temp_line_y_start = player_loc[1] + line_distance_from_ship * math.sin(player_angle + math.radians((360.0/help_lines) * n))
		
		temp_x_stop = player_loc[0] + (line_distance_from_ship + help_line_length) * math.cos(player_angle + math.radians((360.0/help_lines) * n))
		temp_y_stop = player_loc[1] + (line_distance_from_ship + help_line_length )* math.sin(player_angle + math.radians((360.0/help_lines) * n))
	
		helpLines.append( ( (temp_line_x_start, temp_line_y_start), (temp_x_stop, temp_y_stop)) )
		
	#target line
	target_line_start = None
	target_line_stop = None
	target_line_width = 1
	
	if(target_loc != None):
		angle_to_target = math.atan2(target_loc[1],  target_loc[0])
		distance_from_ship = math.sqrt(target_loc[0]**2+target_loc[1]**2)
	
		target_line_start = (	player_loc[0] + line_distance_from_ship * math.cos(angle_to_target),
							player_loc[1] + line_distance_from_ship * math.sin(angle_to_target)
							)
						
		target_line_stop =	(	player_loc[0] + (distance_from_ship + target_line_length) * math.cos(angle_to_target),
							player_loc[1] + (distance_from_ship + target_line_length )* math.sin(angle_to_target)
						)
		pass
	
	#Draw lines
	
	#Draw aim line
	pygame.draw.line(screen, aim_line_colour,  aim_start , aim_stop , aim_line_width)
	
	#Draw help lines
	for lines in helpLines:
		pygame.draw.line(screen, help_line_colour,  lines[0] , lines[1] )
		
	#Draw target line
	if(target_loc != None):
		pygame.draw.line(screen, target_line_colour,  target_line_start , target_line_stop , target_line_width)
	
	return

def drawSpeedLine(screen, ship):
    
    length = float(ship._velocity.magnitude() / config.shipspeed) * config.playerRadiusFromCenter
    
    xx = config.playerRadiusFromCenter*math.cos( ship.shipAngle() + math.radians(180) )
    yy = config.playerRadiusFromCenter*math.sin( ship.shipAngle() + math.radians(180) )
    
    speed_line_start = ship.getRelativeLocation( ship.xy(), ((config.resolution[0] /2 )+xx, (config.resolution[1] /2 )+yy )  )
    speed_line_stop = ( speed_line_start[0] + length * math.cos(ship._velocity.angle() ), speed_line_start[1] + length * math.sin(ship._velocity.angle() ) )
    
    pygame.draw.line(screen, speed_line_colour,  speed_line_start , speed_line_stop , speed_line_width)
    
    return


def drawStats(screen, player, target=None):
	pygame.draw.rect(screen, (0,0,255), pygame.Rect(0,0,config.resolution[0], 50) )
	
	for i in range(0,50):
		if( (i*2) <= player.shieldHealth() ):
			pygame.draw.line(screen, (255,255,0),  (10+i*5, 10) , (10+i*5, 20) , 3 )
		else:
			break
	
	for i in range(0,50):
		if( (i*2) <= player.health() ):
			pygame.draw.line(screen, (100,100,100),  (10+i*5, 25) , (10+i*5, 35) , 3 )
		else:
			break
			
	for i in range(0,50):
		if( (i*2) <= player.energy() ):
			pygame.draw.line(screen, (100,100,0),  (10+i*5, 40) , (10+i*5, 49) , 3 )
		else:
			break
			
	if(target != None):
		for i in range(0,50):
			if( (i*2) <= target.shieldHealth() ):
				pygame.draw.line(screen, (255,255,0),  (300+i*5, 10) , (300+i*5, 20) , 3 )
			else:
				break
	
		for i in range(0,50):
			if( (i*2) <= target.health() ):
				pygame.draw.line(screen, (100,100,100),  (300+i*5, 25) , (300+i*5, 35) , 3 )
			else:
				break
			
		for i in range(0,50):
			if( (i*2) <= target.energy() ):
				pygame.draw.line(screen, (100,100,0),  (300+i*5, 40) , (300+i*5, 49) , 3 )
			else:
				break
	
	
	return	
	
