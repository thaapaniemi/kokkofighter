# coding=ISO-8859-15

import math
import random

import config
import mekaniikka
import world


def checkCollision(o1, o2):
		
	loc = o1.getRelativeLocation(o2.xy(), (0,0) )
				
	distance = math.sqrt(loc[0]**2 + loc[1]**2 )
	rd = o1.radius() + o2.radius()
		
	if(distance < rd):
		return True
	else:
		return False

def collide(o1, o2):
	
	v1 = float( o1._velocity.getVelocity().magnitude())
	v2 = float( o2._velocity.getVelocity().magnitude())  
	a1 = float(o1._velocity.getVelocity().angle() )
	a2 = float(o2._velocity.getVelocity().angle() )
	m1 = float(o1.mass() )
	m2 = float(o2.mass() )

	o1._velocity.set( mekaniikka.Vector(0,0) )
	o2._velocity.set( mekaniikka.Vector(0,0) )
	
	
	f1 =  0.5* v1*m1 / config.timeunit
	f2 =  0.5* v2*m2 / config.timeunit
	
	o1._velocity.effectForce( mekaniikka.Vector(a1 + math.radians(180), f1), m1 )
	o2._velocity.effectForce( mekaniikka.Vector(a2 + math.radians(180), f2), m2 )
	
	o1._velocity.effectForce( mekaniikka.Vector(a2, f2), m1 )
	o2._velocity.effectForce( mekaniikka.Vector(a1, f1), m2 )
	
	while (checkCollision(o1,o2)):
		o1.update()
		

class AI2():
	_ship = None
	_targetList = None
	
	def __init__(self, thisship, targets):
		self._ship = thisship
		self._targetList = targets
		return
		
	def getDistanceToTarget(self):
		xy = self._ship._settings["target"].getRelativeLocation( self._ship.xy() )
		return math.sqrt(xy[0]**2 + xy[1]**2)

	def getAngleToTarget(self):
		xy = self._ship._settings["target"].getRelativeLocation( self._ship.xy() )
		return math.atan2(xy[1], xy[0])
		
	def setTarget(self):
		if(self._targetList != None):
			self._ship._settings["target"] = random.choice(self._targetList)
			while(self._ship._settings["target"] == self._ship and len(self._targetList) > 1 ):
				self._ship._settings["target"] = random.choice(self._targetList)
			if(self._ship._settings["target"] == self._ship):
				self._ship._settings["target"] = None
		else:
			self._ship._settings["target"] = None
		return
	
	def getAngleDifference(self, amount = 0):
		a = self.getAngleToTarget() - self._ship.shipAngle() + math.radians(amount)
		
		while(a > math.radians(360) ):
			a -= math.radians(360)
		
		while(a > math.radians(360) ):
			a += math.radians(360)
			
		return a
	
	def update(self):
		if(self._ship._settings["target"] == None):
			self.setTarget()
		else:
			if(self.getAngleDifference(0)  > 0 ):
				self._ship.increaseAngle()
			else:
				self._ship.decreaseAngle()
		
		if( self.getDistanceToTarget() < 500  and abs(self.getAngleDifference())  < math.radians(10) ):
			#self._ship.speedDown()
			if(self._ship.depleteEnergy(10) == True):
				return self._ship.shoot(True)
		else:
			self._ship.thrust()
		return None
			

#
#class AI():
#
#	re = None
#	values = None
#	
#
#	def __init__(self, values):
#		self.re = ruleEngine.RuleEngine(values)
#		self.values = values
#		self.loadRules("")
#		pass
#		
#	def setTarget(self):
#		temp = random.choice( self.values["all_objects"] )
#		self.values["target"] = temp 
#		
#		
#		
#	def loadRules(self, rulelist):
#		self.re.addRule( ["ai_target == none"], ["SET ai_target random"] )
#		
#		self.re.addRule( ["ai_target != none", "distance_to_target > 100"],
#				["ADD ai_actions turnTowardsTarget", 
#				"ADD ai_actions thrust"] )
#		
#		self.re.addRule( ["ai_target != none", "distance_to_target < 200"],
#				["ADD ai_actions shoot" ] )
#		
#		self.re.addRule( ["ai_target != none", "shield < 10"],
#				["ADD ai_actions turnAwayFromTarget", "ADD ai_actions thrust"] )
#				
#		pass
#		
#	def doRules(self):
#		self.updateValues()
#		
#		print "1. phase"
#		
#		self.re.checkRules()
#		print "2. phase"
#		
#		# Kohteen valinta
#		if(self.values["ai_target"] != None ):
#			if(self.values["ai_target"] == "random"):
#				temp = None
#				while ( temp == self.values["owner"] or temp == None):
#					temp = random.choice( self.values["all_objects"] )
#				self.values["target"] = temp 
#			elif(self.values["ai_target"] == "player"):
#				self.values["target"] = self.values["player"]
#			elif(self.values["ai_target"] == "nearest"):
#				
#				smallest = (2**31, None) #Laitetaan iso arvo l�ht�arvoksi
#				for o in self.values["all_objects"]:
#					temp = [math.sqrt(xy[0]**2 + xy[1]**2), o]
#					if(temp[0] < biggest[0]):
#						smallest = temp
#				
#				self.values["target"] = smallest[1]
#			elif(self.values["ai_target"] == "none"):
#				self.values["target"] = None
#				
#		print "3. phase"
#		
#		#AI:n toiminnot
#		for action in self.values["ai_actions"]:
#			if(action == "turnTowardsTarget"):
#				self.values["currentAngle"] = self.values["angle_to_target"]
#				self.values["newAngle"] = self.values["angle_to_target"]
#				
#			elif(action == "turnAwayFromTarget"):
#				self.values["currentAngle"] = self.values["angle_to_target"] + math.radians(180)
#				self.values["newAngle"] = self.values["angle_to_target"] + math.radians(180)
#			elif(action == "thrust"):
#				self.values["owner"].thrust()
#			elif(action == "speedDown"):
#				self.values["owner"].speedDown()
#			elif(action == "shoot"):
#				self.values["all_objects"].append( self.values["owner"].shoot() )
#
#
#		#Tyhjennet��n arvot seuraavaa kierrosta ajatellen
#		#self.values["ai_target"] = None
#		self.values["ai_actions"] = None
#		
#
#		print "4. phase"
#		
#		pass
#	
#	def updateValues(self):
#		
#		if(self.values["target"] != None):
#			xy = self.values["target"].getRelativeLocation(self.values["owner"].xy() )
#			self.values["distance_to_target"] = math.sqrt(xy[0]**2 + xy[1]**2)
#			self.values["angle_to_target"] = math.atan2(xy[1], xy[0])
#		
#		self.values["shield"] = self.values["owner"]._shieldHealth
#			
#


# Game object, perusolio kaikelle
class GO():
	_settings = {}
	
	_velocity = 0
	
	_ai = None
	
	_ai_enabled = None
	
	def __init__(self, settings, ships = None, ai_enabled = False):
		
		#For ammo class
		self._ai_enabled = ai_enabled
		
		#vaihdetaan nopeudet riippuvaisiksi ruudunp�ivityksest�
		i = config.timeunit
		
		
		self._settings	= settings.copy()
		
		
		self._settings["owner"] = self
		
		self._settings["currentSpeed"]	= i * self._settings["currentSpeed"]
		self._settings["turningSpeed"]	= i * self._settings["turningSpeed"]

		self._settings["currentAngle"]	= i * math.radians (self._settings["currentAngle"])
		self._settings["newAngle"]	= i * math.radians (self._settings["newAngle"])
		
		self._velocity = mekaniikka.Velocity( math.radians(settings["currentAngle"]), settings["currentSpeed"])
		
		self._mass = self._settings["mass"]
		
		#a = self._settings['all_objects'][:]
		
		a = ships
		
		
		self._ai = AI2(self, a )
		


	# Getterit
	def x(self):
		return self._settings["x"]
	
	def y(self):
		return self._settings["y"]
		
	def xy(self):
		return (self._settings["x"], self._settings["y"])
		
	def velocityAngle(self):
		return self._velocity.angle()
		
	def mass(self):
		return self._settings["mass"]
	
	def shipAngle(self):
		return self._settings["newAngle"]
		
	def radius(self):
		return self._settings["radius"]
		
	def getRelativeLocation(self, RLOC, shift = (0,0)):
		x = self._settings["x"] - RLOC[0]  + shift[0]
		y = self._settings["y"] - RLOC[1] + shift[1]
		return ( int( x ), int(y) )
	
	def increaseAngle(self):
		self._settings["newAngle"] += self._settings["turningSpeed"]
		
	def decreaseAngle(self):
		self._settings["newAngle"] -= self._settings["turningSpeed"]
		
	def thrust(self):
		fv = mekaniikka.Vector(self._settings["newAngle"], self._settings["engineForce"])
		self._velocity.effectForce(fv, self._settings["mass"])
		
		while(self._velocity.magnitude() > config.shipspeed):
			self.speedDown()
			
		
	def speedDown(self):
		if (self._velocity.magnitude() > 0.0000001 ):
			fv = mekaniikka.Vector(self._velocity.angle() + math.radians(180), self._settings["engineForce"])
			self._velocity.effectForce(fv, self._settings["mass"])
		else:
			pass

	def update(self, updateTTL = True):
		#t�h�n p�ivitykset
		#muutetaan suuntavektori karteesiseen muotoon
					
		self._settings["x"] += self._velocity.xy()[0]
		self._settings["y"] += self._velocity.xy()[1]
		
		if( updateTTL == True):
			if(self._settings["ttl"] > 0 ):
				self._settings["ttl"] -= 1
			elif(self._settings["ttl"] == 0):
				return False
			else:
				return True

		if(self._settings["target"] != None):
			pass
			#if(self._settings["target"]._hullHealth <= 0 ):
			#	self._settings["target"] = None
		
		
	def shoot(self, smart = False):
		r = config.r_angle * (random.random() - 0.5) * 2
		temp = {
			"x": self.x(),
			"y": self.y(),
			"currentAngle": math.degrees(self.shipAngle()) + r,
			"currentSpeed": config.shipspeed + 1,
			"mass": 10,
			"turningSpeed": math.radians(360),
			"newAngle": math.degrees(self.shipAngle()) +r,
			"radius": 3,
			"ttl": config.fps*2 ,
			"engineForce": 0.01,
			"target": self._settings["target"]
			}
			
			
		temp = Ammo(temp, [ self ], smart )
		temp._velocity.addVelocity(self._velocity)
		
		while( checkCollision(self, temp) ):
			temp.update(False)
			
		return temp
	
	def collide(self):
		return True
		pass
		
	def health(self):
		return -1
		
class Ammo(GO):
	
	def energy(self):
		return 10
	
	def thrust(self):
		return

	def collide(self):
		return False
		pass
	def update(self, updateTTL = True):
		#t�h�n p�ivitykset
		#muutetaan suuntavektori karteesiseen muotoon
					
		self._settings["x"] += self._velocity.xy()[0]
		self._settings["y"] += self._velocity.xy()[1]
		
		if( updateTTL == True):
			if(self._settings["ttl"] > 0 ):
				self._settings["ttl"] -= 1
			elif(self._settings["ttl"] == 0):
				return False
			else:
				return True

		if(self._settings["target"] != None and self._ai_enabled == True):
			self._ai.update()
			self._velocity._angle = self._settings["newAngle"]
			
			pass
			
	def depleteEnergy(self, amount):
		return False

 # Ship Class
class Ship(GO):
	_hullHealth = 100.00
	_shieldHealth = 100.00
	_energy = 100.00
	
	def collide(self):
		returntemp = True
		
		self._shieldHealth -= 10
		
		if(self._shieldHealth < 0):
			self._shieldHealth = 0
			self._hullHealth -= 10
			if(self._hullHealth < 0):
				returntemp = False
		
		return returntemp
		
	def generateShield(self):
		if(self._shieldHealth < 100):
			self._shieldHealth += float (1.0/config.fps)
		
	def generateEnergy(self):
		if(self._energy < 100):
			self._energy += float (20.0 / config.fps)
	
	
	def depleteEnergy(self, amount):
		if(amount < self._energy):
			self._energy -= amount
			return True
		else:
			return False
		
	def health(self):
		return self._hullHealth
		
	def shieldHealth(self):
		return self._shieldHealth
		
	def energy(self):
		return self._energy

