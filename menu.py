# coding=ISO-8859-15

import pygame
import config
import world

from pygame.locals import *

START = 0
QUIT = 1

def menu(screen0):
	surfaces = []
	s_surfaces = []

	menu_surface = pygame.image.load("graphics/menu.png")
	screen = menu_surface
	

	surfaces.append(pygame.image.load("graphics/start.png"))
	surfaces.append(pygame.image.load("graphics/quit.png"))

	s_surfaces.append(pygame.image.load("graphics/start_s.png"))
	s_surfaces.append(pygame.image.load("graphics/quit_s.png"))
	
	#screen.blit(menu_surface, (0,0))
	
	pygame.display.flip()
	
	
	done = False
	keysDown = set()
	select = 0
	
	pygame.time.wait(1000)
	
	a = "HiScore: " + str( world.readHSfromFile() )
	
	font = pygame.font.Font(None, 72)
	hiscore = font.render(a, True, (0,255,0), (100, 100, 255) )
	
	while not done:


		for event in pygame.event.get():
			if event.type == KEYDOWN:
				keysDown.add(event.key)
			elif event.type == KEYUP:
				if event.key in keysDown:
					keysDown.remove(event.key)
			elif event.type == QUIT:
				done = True
	
				
		if K_UP in keysDown:
			if(select > 0):
				select -= 1
			
			
			pass
		elif K_DOWN in keysDown:
			if(select < len(surfaces)-1 ) :
				select += 1
			
			pass
			
			
		elif K_ESCAPE in keysDown:
			select = len(surfaces)-1
			done = True
		
		elif K_RETURN in keysDown:
			done = True
		
		
		#screen.blit(menu_surface, (0,0))
		screen.blit(hiscore, (100, 100) )
	
		for s in surfaces:
			screen.blit(s, (250,250 + surfaces.index(s)*50 ))
		
		if(select >= 0):
			screen.blit(s_surfaces[select], (250,250 + select*50 ))
			#pygame.display.flip()

		pygame.transform.smoothscale(screen, config.resolution, screen0)
		pygame.display.flip()


	return select
	
	